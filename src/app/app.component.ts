import { Component } from '@angular/core';

// Services
import { GlobalService } from './services/global.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'app';
  errorMessage: any;

  constructor(private globalSrv: GlobalService) {
    // Nos suscribimos al observable (evento), cuando hagamos un next() en otro componente
    // se activará y mostrará el mensaje
    this.globalSrv.getMessage().subscribe(data => this.errorMessage = data);
  }
}
