import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

// Internal Modules
import { AppRoutingModule } from './app-routing.module';
import { ComponentsModule } from './components/components.module';

// Services
import { GlobalService } from './services/global.service';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    AppRoutingModule,
    ComponentsModule
  ],
  providers: [ GlobalService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
