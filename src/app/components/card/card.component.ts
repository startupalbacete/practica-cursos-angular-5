import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Router} from '@angular/router';
import { ApiService } from './../../services/api.service';
import { GlobalService } from '../../services/global.service';

import { EndPoints } from './../../app.constants';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() data;
  @Output() recargar = new EventEmitter;

  constructor(private api: ApiService, private globalSrv: GlobalService, private router: Router) { }

  ngOnInit() {
  }

  verCurso(idCurso) {
    this.router.navigate(['cursos/ver',idCurso],{skipLocationChange: true});
  }

  editarCurso(idCurso) {
    this.router.navigate(['cursos/editar',idCurso],{skipLocationChange: true});
  }

  borrarCurso() {
    this.api.delCurso(EndPoints.cursos + EndPoints.slash, this.data.id)
      .subscribe(
        () => this.onComplete() // onComplete o finally del observable
      );
  }

  onComplete() {
    // Emitimos evento al padre para recargar la lista de cursos
    this.recargar.emit();
    // Vamos a disparar el observable cuando se complete la petición al backend.
    this.globalSrv.sendMessage('Curso eliminado con exito.', 'alert-danger');
  }

}
