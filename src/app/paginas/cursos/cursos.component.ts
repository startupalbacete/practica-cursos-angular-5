import { Component, OnInit } from '@angular/core';

// Services
import { ApiService } from './../../services/api.service';

// Constantes
import { EndPoints } from './../../app.constants';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})
export class CursosComponent implements OnInit {

  cursos;

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.api.getCursos(EndPoints.cursos)
      .subscribe(data => this.cursos = data);
  }

  search(value) {
    console.log(value);
    if(value) {
      this.api.getCursosFiltrados(EndPoints.cursos, value)
        .subscribe(response => {
            this.cursos = response;
        }, error => console.log(error));
    } else {
      this.getData();
    }
  }

}
