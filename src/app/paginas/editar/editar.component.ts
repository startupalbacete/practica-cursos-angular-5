import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../services/global.service';

import { EndPoints } from './../../app.constants'

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {

  curso:any = {
    id: 0,
    name: '',
    descripcion: '',
    max_alumnos: 0,
    horas: 0,
    plazas_disponibles: 0,
    precio: 0
  };

  constructor(private route:ActivatedRoute, private api: ApiService, private globalSrv: GlobalService, private router: Router) {
    this.route.params.subscribe(params => {
      this.curso.id = params['id']
      this.getData(this.curso.id);
    });
  }

  ngOnInit() {
  }

  getData(idCurso) {
    this.api.getCurso(EndPoints.cursos + EndPoints.slash, idCurso)
      .subscribe(data => this.curso = data);
  }

  onSubmit(form) {
    this.api.actualizarCurso(EndPoints.cursos + EndPoints.slash,form.form.value)
      .subscribe(
        response => console.log('post: '+JSON.stringify(response)), // next del observable
        err => console.log(err), // error del observable
        () => this.onComplete() // onComplete o finally del observable
      );
  }

  onComplete() {
    // Vamos a disparar el observable cuando se complete la petición al backend.
    this.router.navigateByUrl('cursos');
    this.globalSrv.sendMessage('El curso se ha guardado con exito.', 'alert-success');
  }


}
