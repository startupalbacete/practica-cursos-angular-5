import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { GlobalService } from '../../services/global.service';

import { EndPoints } from './../../app.constants'

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.css']
})
export class NuevoComponent implements OnInit {

  name;
  descripcion;
  max_alumnos;
  precio;
  horas;
  plazas_disponibles;

  constructor(private api: ApiService, private globalSrv: GlobalService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(form) {
    this.api.setCurso(EndPoints.cursos + EndPoints.slash,form.form.value)
      .subscribe(
        response => console.log('post: '+JSON.stringify(response)), // next del observable
        err => console.log(err), // error del observable
        () => this.onComplete() // onComplete o finally del observable
      );
  }

  onComplete() {
    // Vamos a disparar el observable cuando se complete la petición al backend.
    this.router.navigateByUrl('cursos');
    this.globalSrv.sendMessage('Nuevo curso creado con exito.', 'alert-success');
  }

}
