import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

// Formularios
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

// Internal Modules
import { ComponentsModule } from './../components/components.module';

// Services
import { ApiService } from './../services/api.service';

// components
import { CursosComponent } from './cursos/cursos.component';
import { VerComponent } from './ver/ver.component';
import { EditarComponent } from './editar/editar.component';
import { NuevoComponent } from './nuevo/nuevo.component';

export const ROUTES: Routes = [
  { path: '',component: CursosComponent },
  { path: 'ver/:id',component: VerComponent },
  { path: 'editar/:id',component: EditarComponent },
  { path: 'nuevo',component: NuevoComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    ComponentsModule,
    HttpModule,
    FormsModule
  ],
  declarations: [
    CursosComponent,
    EditarComponent,
    VerComponent,
    NuevoComponent
  ],
  providers: [
    ApiService
  ]
})
export class PaginasModule { }
