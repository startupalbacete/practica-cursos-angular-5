import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ApiService } from './../../services/api.service';
import { EndPoints } from './../../app.constants';

@Component({
  selector: 'app-ver',
  templateUrl: './ver.component.html',
  styleUrls: ['./ver.component.css']
})
export class VerComponent implements OnInit {

  idCurso;
  curso;

  constructor(private router: ActivatedRoute, private api: ApiService) {
    this.router.params
      .subscribe(params => this.idCurso = params['id']);
  }

  ngOnInit() {
    this.getData(this.idCurso);
  }

  getData(idCurso) {
    this.api.getCurso(EndPoints.cursos + EndPoints.slash, idCurso)
      .subscribe(data => this.curso = data);
  }

}
