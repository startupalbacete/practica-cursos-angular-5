import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';

import { CONFIG } from './../app.constants';

@Injectable()
export class ApiService {

  constructor(private http: HttpClient, private httpAntiguo:Http) { }

  getCursos(endpoint) {
    return this.http.get(CONFIG.apiUrl + endpoint);
  }

  getCursosFiltrados(endpoint, value = '') {
    const params = new URLSearchParams();
    const filterJson = {
      'or': [
        {'name': {'operator':'fuzzy', 'value':value}}
      ]
    };
    params.set('filter', JSON.stringify(filterJson));
    return this.httpAntiguo.get(CONFIG.apiUrl + endpoint, {params})
      .map(response => response.json());
  }

  getCurso(endpoint, idCurso) {
    return this.http.get(CONFIG.apiUrl + endpoint + idCurso);
  }

  setCurso(endpoint, curso) {
    return this.http.post(CONFIG.apiUrl + endpoint, curso);
  }

  actualizarCurso(endpoint, curso) {
    console.log('curso', curso);
    return this.http.put(CONFIG.apiUrl + endpoint + curso.id, curso);
  }

  delCurso(endpoint, idCurso) {
    return this.http.delete(CONFIG.apiUrl + endpoint + idCurso);
  }

}
