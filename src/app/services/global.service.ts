import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

/**
* @description
* Con las funciones sendMessage(), clearMessage() y getMessage()
* pretendemos conseguir manupular información desde cualquier componente
* a traves de observables y suscripciones.
*
* Todos los componentes que esten suscritos al observable getMessage() recibirán
* información cuando ejecutemos una función de next().
*
* Ejecución del next():
* - nuevo.component.ts
* - editar.component.ts
* - card.component.ts
*
* Escucha del next():
* - app.components.ts
*
* Si en algún componente no queremos que siga escuchando el evento podemos utilizar
* unsubscribe() para no escucharlo.
*
* Más información de observables.
* @tutorial {http://reactivex.io/rxjs/}
*
*/



@Injectable()
export class GlobalService {

  // Creamos el objeto subject para la gestión del observable y sus suscripciones.
  private showErrorSubject = new Subject<any>();
  // Creamos el objeto observable al que se suscribiran los componentes donde enviaremos información
  public showErrorObservable = this.showErrorSubject.asObservable();

  constructor() { }

  // Función para enviar un "mensaje" a todos los componentes suscritos a getMessage()
  sendMessage(message: string, clase: string) {
    this.showErrorSubject.next({ text: message, class: clase });
    setTimeout(() => {
      this.clearMessage();
    }, 3000);
  }

  // Función para enviar un "mensaje vacio" a todos los componentes suscritos a getMessage()
  clearMessage() {
    this.showErrorSubject.next();
  }

  // Observable (evento) al que se suscribiran nuestros componentes y estarán esperando
  // la orden de next
  getMessage(): Observable<any> {
    return this.showErrorObservable;
  }

}
